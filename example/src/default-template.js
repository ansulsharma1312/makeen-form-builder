export const DefaultTemplate = {
  fields: [
    [
      {
        type: 'select',
        label: 'Title',
        placeholder: 'Title',
        datafield: 'selectTitle',
        id: 'af9a2335-0555-4f24-b8c8-25dd5deda46f',
        options: [
          { name: '', value: '' },
          { name: 'Master', value: 'mst' },
          { name: 'Mr.', value: 'mr' },
          { name: 'Ms.', value: 'ms' },
          { name: 'Mrs.', value: 'mrs' }
        ]
      },
      {
        type: 'text',
        label: 'First Name',
        placeholder: 'Your first name',
        datafield: 'firstName',
        id: 'af9a2335-0555-4f24-b8c8-25dd5deda4334s',
        required: true
      },
      {
        type: 'text',
        label: 'Middle Name',
        placeholder: 'Your Middle name',
        datafield: 'middleName',
        id: 'af9a2335-0555-4f24-b8c8-25dd5deda46g'
      },
      {
        type: 'text',
        label: 'Last Name',
        placeholder: 'Your Last name',
        datafield: 'lastName',
        id: 'af9a2335-0555-4f24-b8c8-25dd5deda46h',
        required: true
      }
    ],
    [
      {
        type: 'text',
        label: 'Primary Contact Number',
        placeholder: 'Primary Contact Number',
        datafield: 'primaryContactNumber',
        id: 'af9a2335-0555-4f24-b8c8-25dd5deda46i',
        props: {type: 'mobile'},
        required: true
      },
      {
        type: 'text',
        label: 'Email',
        placeholder: 'Email Id',
        datafield: 'emailId',
        id: 'af9a2335-0555-4f24-b8c8-25dd5deda46g',
        props: {type: 'email'},
        required: true
      }
    ],
    [
      {
        type: 'text',
        multiline: true,
        label: 'Current Address',
        placeholder: 'Full Address',
        datafield: 'currentAddress',
        id: 'af9a2335-0555-4f24-b8c8-25dd5deda46i'
      }
    ],
    [
      {
        type: 'imageupload',
        multiline: true,
        label: 'Your Photo',
        datafield: 'photo',
        id: 'af9a2335-0555-4f24-b8c8-25dd5deda46d'
      }
    ],
    [
      {
        type: 'checkbox',
        label: 'I agree to the terms and conditions',
        datafield: 'agreeToTheTerms',
        id: 'af9a2335-0555-4f24-b8c8-25dd5dtda46d',
        required: true
      }
    ]
  ]
}