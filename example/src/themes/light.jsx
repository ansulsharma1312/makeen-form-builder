export const light = {
    meta: {
      name: 'light',
      displayName: 'light'
    },
    colors: {
      background: '#fdfdfd',
      background1: '#f6f6f6',
      background2: '#838383',
      background3: '#fdfdfd',
      border: '#010507',
      input: {
        border: '#C6C6C6',
        placeholder: '#ffffff',
        color: '#000',
        disabled: '#787878'
      },
      error: {
        color: 'yellow'
      },
      primaryButton: '#000',
      primaryButtonText: '#000',
      primaryText: '#000',
      secondaryText: '#000',
      ternaryText: '#000',
      listPrimary: '#000',
      listSecondary: '#000',
      card: {
        start: '#d8d8d8',
        end: '#d8d8d8',
        border: '#202125',
        title: '#e8e8e8',
        shadow: 'transparent'
      },
      link: {
        color: '#A5A5A5'
      },
      button: {
        color: '#01B075',
        secondary: '#9e9e9e',
        danger: '#ff1744',
        link: '#bec6f4',
        border: 'red'
      },
      text: '#e8e8e8',
      toggle: {
        color: '#01B075',
        bg: '#FFFFFF',
        colorActive: '#FFFFFF',
        bgActive: '#01B075'
      },
      checks: {
        color: '#01B075',
        bg: '#FFFFFF',
        colorActive: '#FFFFFF',
        bgActive: '#01B075'
      }
    }
  }
  