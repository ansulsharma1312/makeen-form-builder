import React, { useEffect } from 'react'

import FormBuilder from 'makeen-form-builder'
import { ThemeProvider } from 'styled-components'
import { Themes } from './themes'
import { DefaultTemplate } from './default-template'

import Typography from '@mui/material/Typography'
import Modal from '@mui/material/Modal'

import Box from '@mui/material/Box'
import Button from '@mui/material/Button'

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 4
}

const App = () => {
  const [template, setTemplate] = React.useState(DefaultTemplate)
  const [themeName, setThemeName] = React.useState('dark')
  const [data, setData] = React.useState({
    'F_89186732-19d3-4271-b133-ed92757cb17': true,
    'F_89186732-19d3-4271-b133-ed92727cb7d1': 'mr'
  })
  const [formState, setFormState] = React.useState(DefaultTemplate)

  const [openPreview, setOpenPreviewDialog] = React.useState(false)
  const [openValidate, setOpenValidateModal] = React.useState(false)

  useEffect(() => {
    // setThemeName()
  }, [themeName])

  const handleOpenPreview = () => setOpenPreviewDialog(true)
  const handleClosePreview = () => setOpenPreviewDialog(false)

  const handleOpenValidate = () => setOpenValidateModal(true)
  const handleCloseValidate = () => setOpenValidateModal(false)

  const onCloseModal = () => {
    setOpenValidateModal(false);
    setOpenPreviewDialog(false);
  }

/*   const handleChange = (event, newAlignment) => {
    setThemeName(newAlignment)
  } */
  return (
    <ThemeProvider theme={Themes[themeName]}>
      <div className='container' style={{ boxSizing: 'border-box' }}>
        <div style={{ padding: '1rem', display: 'flex' }}>
          <div style={{ flex: 1 }}>
            <h1 style={{ fontWeight: 100 }}>Makeen Form Builder</h1>
          </div>
          <div>
          <div>
              <Button onClick={handleOpenValidate}>Validate</Button>
            </div>
            <div>
              <Button onClick={handleOpenPreview}>Preview</Button>
            </div>
          </div>
        </div>
        <div className='form-wrapper'>
          <FormBuilder.FormPlanner
            config={{
              showPreview: true
            }}
            onFormValueChanged={(val) => {
              setFormState(val)
              console.log(`Form value: ${JSON.stringify(val)}`)
              setTemplate(val)
            }}
            baseTheme={themeName}
            themeOverride={Themes[themeName]}
            fieldTemplate={template}
            validate={openValidate}
            onCloseModal={onCloseModal}
          />
        </div>
      </div>

      <Modal
        open={openPreview}
        onClose={handleClosePreview}
        aria-labelledby='modal-modal-title'
        aria-describedby='modal-modal-description'
      >
        <Box
          sx={style}
          style={{
            width: '90vw',
            height: '90vh',
            overflow: 'auto'
          }}
        >
          <Typography
            id='modal-modal-title'
            variant='h6'
            component='h2'
            style={{ color: '#000' }}
          >
            Preview Form
          </Typography>
          {openPreview ? (
            <FormBuilder.FormViewer
              id='example-form'
              baseTheme={'dark'}
              themeOverride={Themes[themeName]}
              onChange={(k, v, c) => {
                // data[k] = v
                // setData(data)
                console.log('Form data changed')
                console.log(JSON.stringify(data))
              }}
              onControlValueChanged={(k, v, f) => {
                console.log('Control data changed')
                data[k] = v
                setData(data)
                console.log(
                  `Field: ${k} Value: ${v} Field: ${JSON.stringify(f)}`
                )
              }}
              data={data}
              template={formState}
            />
          ) : null}
        </Box>
      </Modal>

      {/* <Modal
        open={handleOpenValidate}
        clo={handleClosePreview}
        aria-labelledby='modal-modal-title'
        aria-describedby='modal-modal-description'
      >
        <Box
          sx={style}
          style={{
            width: '90vw',
            height: '90vh',
            overflow: 'auto'
          }}
        >
          <Typography
            id='modal-modal-title'
            variant='h6'
            component='h2'
            style={{ color: '#000' }}
          >
            Validate
          </Typography>
          {openValidate ? (
            <FormBuilder.FormViewer
              id='example-form'
              baseTheme={'dark'}
              themeOverride={Themes[themeName]}
              onChange={(k, v, c) => {
                // data[k] = v
                // setData(data)
                console.log('Form data changed')
                console.log(JSON.stringify(data))
              }}
              onControlValueChanged={(k, v, f) => {
                console.log('Control data changed')
                data[k] = v
                setData(data)
                console.log(
                  `Field: ${k} Value: ${v} Field: ${JSON.stringify(f)}`
                )
              }}
              data={data}
              template={formState}
              validate={}
            />
          ) : null}
        </Box>
      </Modal> */}
    </ThemeProvider>
  )
}
export default App
