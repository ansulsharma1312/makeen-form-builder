import * as React from 'react'

import {
  FaCaretSquareDown,
  FaFilePdf,
  FaFont,
  FaGripLines,
  FaICursor,
  FaTh,
  FaUpload,
  FaCheckSquare,
  FaCircle
} from 'react-icons/fa'

const AllControls = [
  {
    type: 'label',
    icon: FaFont,
    name: 'label',
    display: 'Label',
    group: 'Form Components',
    default: true
  },
  {
    type: 'text',
    icon: FaICursor,
    name: 'textbox',  
    display: 'Text Box',
    group: 'Form Components',
    bordered: true,
    default: true
  },
  {
    type: 'checkbox',
    icon: FaCheckSquare,
    name: 'checkbox',
    display: 'Checkbox',
    group: 'Form Components'
  },
  {
    type: 'select',
    icon: FaCaretSquareDown,
    name: 'select',
    display: 'Select',
    group: 'Form Components',
    default: true
  },
  {
    type: 'radio',
    icon: FaCircle,
    name: 'radio',
    display: 'Radio',
    group: 'Form Components',
    bordered: true,
    multiline: true
  },
  {
    type: 'divider',
    icon: FaGripLines,
    name: 'divider',
    display: 'Divider',
    group: 'Form Components',
    default: true
  },

  {
    type: 'pdf',
    icon: FaFilePdf,
    name: 'pdf',
    display: 'PDF',
    group: 'Form Components'
  },
  {
    type: 'imageupload',
    icon: FaUpload,
    multiple: false,
    name: 'imageupload',
    display: 'Image',
    group: 'Form Components',
    accept: '.jpg,.jpeg,.png',
    bordered: true,
    multiline: true
  }
]

const groupBy = function (xs, key) {
  return xs.reduce(function (rv, x) {
    ;(rv[x[key]] = rv[x[key]] || []).push(x)
    return rv
  }, {})
}

export const getControls = (requestedControls) => {
  if (requestedControls && requestedControls.length) {
    const finalResult = []
    let sendingDefault = true
    requestedControls.forEach((rc) => {
      if (rc) {
        sendingDefault = false
        const foundEntry = AllControls.filter((ac) => {
          return ac.name === rc.name
        })
        if (foundEntry && foundEntry.length) {
          finalResult.push(Object.assign(foundEntry[0], rc))
        }
      }
    })
    if (!sendingDefault) {
      return groupBy(finalResult, 'group')
    } else {
      return groupBy(AllControls, 'group')
    }
  } else {
    return groupBy(AllControls, 'group')
  }
}

export const getAllInNameFormat = () => {
  const res = []

  AllControls.forEach((c) => {
    res.push({ name: c.name })
  })
  return res
}
