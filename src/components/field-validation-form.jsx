import {
  MakeenFormControlEdit,
  MakeenFormFormRow,
  MakeenFormToolButton,
  FVPlannerWrapper,
  MakeenFormDrawer,
  MakeenFormPaperVerticalPadding,
  MakeenFormSquareActionButton,
  MakeenFormDividerField,
  MakeenFormFormColumn,
  MakeenFormHeaderField
} from './styled'
import {
  MakeenFormDataGridView,
  FVFileUploadField,
  FVFormCheckboxField,
  FVFormNonField,
  FVFormRadioField,
  FVFormSelectField,
  FVFormTextAreaField,
  FVFormTextField,
  FVImageloadField
} from './form-fields'
import { Field, reduxForm } from 'redux-form'
import React, { Fragment, useEffect, useState } from 'react'
import {
  FaChevronRight,
  FaChevronLeft,
  FaChevronDown,
  FaChevronUp,
  FaClone,
  FaPenAlt,
  FaTrashAlt,
  FaPen,
  FaEllipsisH
} from 'react-icons/fa'
import { Button, Grid } from '@mui/material'
import { formValueSelector } from "redux-form";
import { connect } from "react-redux";
/* import store from "../store/store" */

const ValidationMap = {
  required: (value) => (value ? undefined : 'Required'),
  maxLength: (max) => (value) =>
    value && value.length > max
      ? `Must be ${max} characters or less`
      : undefined,
  number: (value) =>
    value && isNaN(Number(value)) ? 'Must be a number' : undefined,
  minValue: (min) => (value) =>
    value && value < min ? `Must be at least ${min}` : undefined,
  email: (value) =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
      ? 'Invalid email address'
      : undefined,
  mobile: (value) =>
    value && !/^(\+\d{1,3}[- ]?)?\d{10}$/.test(value)
        ? 'Invalid mobile number'
        : undefined,
  tooOld: (value) =>
    value && value > 65 ? 'You might be too old for this' : undefined,
  aol: (value) =>
    value && /.+@aol\.com/.test(value)
      ? 'Really? You still use AOL for your email?'
      : undefined
}
const renderSwitch = (type) => {
  switch (type) {
    case 'text':
      return FVFormTextField
    case 'select': 
      return FVFormSelectField
    case 'radio':
      return FVFormRadioField
    case 'textarea':
      return FVFormTextAreaField
    case 'checkbox':
      return FVFormCheckboxField
    case 'grid':
      return MakeenFormDataGridView
    case 'fileupload':
      return FVFileUploadField
    case 'imageupload':
      return FVImageloadField
    default:
      return 'foo'
  }
}

/* const validate = (values, props) => {
  const errors = {}
  // if (!values.username) {
  //   errors.username = 'Required'
  // }
  // if (!values.password) {
  //   errors.password = 'Required'
  // }
  return errors
} */


let FieldLevelValidationForm = ({
  data,
  fields,
  onChange,
  theme,
  controls,
  onAddColumn,
  plannerConfig,
  controlMarker = {},
  validator,
  ...props
}) => {
  const { handleSubmit, pristine, reset, submitting } = props
  const [controlsState, setControlsSet] = useState({})
  const [localFields, setLocalFields] = useState([])
  let [errorData, setErrorData] = useState([])
  const [selectedIndices, setSelectedIndices] = useState({})
  const [drawerOpen, setDrawerOpen] = useState(false)
  const [addAdditional, setAddAdditional] = useState('')

  useEffect(() => {
    const res = {}
    setLocalFields(JSON.parse(JSON.stringify(fields)))
    fields.forEach((fldRow) => {
      fldRow.forEach((fld) => {
        if (fld.datafield && data[fld.datafield] !== undefined) {
          res[fld.datafield] = data[fld.datafield]
        }
      })
    })
    setControlsSet({ ...res })
  }, [fields, data])

  const getErrorObj = (key, value, field) => {
    if (!validator || key === 'props') return;
    const label = field.label;

    let error = { [label] : {}};
    const index = errorData.findIndex(obj => Object.keys(obj).includes(label));
    if (index > -1) {
      errorData.splice(index, 1);
    }
    if (!value && field.required && ValidationMap.required(field.isRequired)) {
        error[label]['isRequired'] = 'required';
    } else if (field.props?.type === 'email' && ValidationMap.email(value)) {
        error[label]['error'] = ValidationMap.email(value);
    } else if (field.props?.type === 'number' && ValidationMap.number(value)) {
        error[label]['error'] = ValidationMap.number(value);
    } else if (field.props?.type === 'mobile' && ValidationMap.mobile(value)) {
        error[label]['error'] = ValidationMap.mobile(value);
    } else if (field.props?.minValue && ValidationMap.minValue(parseInt(value))) {
        error[label]['error'] = ValidationMap.minValue(parseInt(value));
    } else if (field.validation && !validation.test(value)) {
        error[label]['error'] = 'Error in Validating Regular Expression';
    }
    if (error[label]['isRequired'] || error[label]['error']) {
      errorData.push(error)
      setErrorData(errorData)
    }
    
  }

  const onValueChanged = (key, value, field) => {
    controlsState[key] = value
    setControlsSet({ ...controlsState })
    console.log(controlsState);
    getErrorObj(key, value, field);
    if (onChange) {
      onChange(key, value, field, errorData)
    }
  }
  const getValidations = (field) => {
    const res = []
    if (field.validations && field.validations.length) {
      Object.keys(field.validations).forEach((k) => {
        if (ValidationMap[k]) {
          res.push(ValidationMap[k])
        }
      })
    }
    return res
  }

  const onActionButtonClick = (type, row, column, e, closeDrawer) => {
    e.preventDefault()
    e.stopPropagation()
    if (closeDrawer) {
      setDrawerOpen(false)
    }
    if (props.onButtonClick) {
      props.onButtonClick(type, row, column)
    }
  }

  const onMoreClick = (row, column, columncount, rowcount) => {
    setSelectedIndices({
      rowIndex: row,
      columnIndex: column,
      columnCount: columncount,
      rowCount: rowcount
    })
    setDrawerOpen(true)
  }

  const getSelectedLabel = () => {
    if (selectedIndices.rowIndex > -1 && selectedIndices.columnIndex > -1) {
      try {
        return localFields[selectedIndices.rowIndex][
          selectedIndices.columnIndex
        ].label
      } catch {}
    }
    return ''
  }

  return (
    <div>
      <form onSubmit={handleSubmit}>
      <div style={{ flex: 1 }}>
        <FVPlannerWrapper container spacing={1}>
          <React.Fragment>
            {localFields.map((fldrow, fldrowi) => {
              return (
                <Grid
                  container
                  key={fldrowi}
                  // spacing={{ xs: 2, md: 3 }}
                  // columns={{ xs: 4, sm: 8, md: 12 }}
                >
                  {/* <div  style={{ width: '100%', display: 'flex' }}> */}
                  {fldrow.map((fld, fldi) => {
                    return (
                      <Grid
                        item
                        xs={12}
                        sm={fldrow.length > 1 ? 6 : 12}
                        md={
                          fldrow.length > 1 ? (fldrow.length < 3 ? 6 : 4) : 12
                        }
                        lg={fldrow.length > 1 ? 12 / fldrow.length : 12}
                        key={`${fldrowi}-${fldi}`}
                      >
                        {/* <div
                        style={{ width: '100%', padding: '4px' }}
                        key={`${fldrowi}-${fldi}`}
                      > */}
                        {fld.visible !== false ? (
                          <MakeenFormFormRow
                            editable={props.editable}
                            bordered={fld.bordered}
                          >
                            <MakeenFormFormColumn
                              editable={props.editable}
                              selected={
                                selectedIndices.rowIndex === fldrowi &&
                                selectedIndices.columnIndex === fldi
                              }
                              className='element-wrapper'
                            >
                              <div className='edit-overlay' />
                              {fld.datafield ? (
                                <Field
                                  name={fld.datafield}
                                  component={renderSwitch(fld.type)}
                                  field={fld}
                                  themeOverride={theme}
                                  required={
                                    fld.validations && fld.validations.required
                                  }
                                  inputvalue={controlsState[fld.datafield]}
                                  allvalue={controlsState}
                                  onValueChanged={(key, value, field) =>
                                    onValueChanged(key, value, field)
                                  }
                                  editable={props.editable}
                                  input={{
                                    value: controlsState[fld.datafield]
                                  }}
                                  {...props}
                                />
                              ) : (
                                <FVFormNonField field={fld} />
                              )}{' '}
                            </MakeenFormFormColumn>
                            <MakeenFormControlEdit
                              className='control-edit-overlay'
                              {...{
                                selected: props.selectedControlIndex === fldrowi
                              }}
                              key={`${fldrowi}-${fldi}`}
                            >
                              <div className='content-details action-button-wrapper fadeIn-bottom'>
                                {props.editable ? (
                                  <Fragment>
                                    <MakeenFormToolButton
                                      variant='contained'
                                      size='small'
                                      aria-label='clone'
                                      onMouseEnter={(e) => {
                                        e.preventDefault()
                                        e.stopPropagation()
                                      }}
                                      onClick={(e) =>
                                        onActionButtonClick(
                                          'ed',
                                          fldrowi,
                                          fldi,
                                          e
                                        )
                                      }
                                    >
                                      <FaPenAlt/>
                                    </MakeenFormToolButton>
                                    <MakeenFormToolButton
                                      variant='contained'
                                      color='secondary'
                                      size='small'
                                      aria-label='delete'
                                      onMouseEnter={(e) => {
                                        e.preventDefault()
                                        e.stopPropagation()
                                      }}
                                      onClick={(e) =>
                                        onActionButtonClick(
                                          'rm',
                                          fldrowi,
                                          fldi,
                                          e
                                        )
                                      }
                                    >
                                      <FaTrashAlt/>
                                    </MakeenFormToolButton>
                                    <MakeenFormToolButton
                                      variant='contained'
                                      size='small'
                                      aria-label='more'
                                      onMouseEnter={(e) => {
                                        e.preventDefault()
                                        e.stopPropagation()
                                      }}
                                      onClick={(e) =>
                                        onMoreClick(
                                          fldrowi,
                                          fldi,
                                          fldrow.length,
                                          localFields.length
                                        )
                                      }
                                    >
                                      <FaEllipsisH/>
                                    </MakeenFormToolButton>
                                  </Fragment>
                                ) : null}
                              </div>
                            </MakeenFormControlEdit>
                          </MakeenFormFormRow>
                        ) : (
                          <div></div>
                        )}
                      </Grid>
                    )
                  })}
                </Grid>
              )
            })}
          </React.Fragment>
        </FVPlannerWrapper>
      </div>
      <MakeenFormDrawer
        anchor={'right'}
        open={drawerOpen}
        onClose={() => setDrawerOpen(false)}
      >
        {drawerOpen ? (
          <MakeenFormPaperVerticalPadding>
            <MakeenFormHeaderField>More Option: {getSelectedLabel()}</MakeenFormHeaderField>
            <MakeenFormDividerField />
            <div style={{ display: 'flex' }}>
              <MakeenFormSquareActionButton
                disabled={selectedIndices.rowIndex === 0}
                onClick={(e) => {
                  setSelectedIndices({
                    ...selectedIndices,
                    rowIndex: selectedIndices.rowIndex + -1
                  })
                  onActionButtonClick(
                    'mu',
                    selectedIndices.rowIndex,
                    selectedIndices.columnIndex,
                    e,
                    false
                  )
                }}
              >
                <div>
                  <FaChevronUp/>
                </div>
                <div>Move Up Row</div>
              </MakeenFormSquareActionButton>
              <MakeenFormSquareActionButton
                disabled={
                  selectedIndices.rowIndex === selectedIndices.rowCount - 1
                }
                onClick={(e) => {
                  setSelectedIndices({
                    ...selectedIndices,
                    rowIndex: selectedIndices.rowIndex + 1
                  })
                  onActionButtonClick(
                    'md',
                    selectedIndices.rowIndex,
                    selectedIndices.columnIndex,
                    e,
                    false
                  )
                }}
              >
                <div>
                  <FaChevronDown/>
                </div>
                <div>Move Down Row</div>
              </MakeenFormSquareActionButton>
              <MakeenFormSquareActionButton
                disabled={selectedIndices.columnIndex === 0}
                onClick={(e) => {
                  setSelectedIndices({
                    ...selectedIndices,
                    columnIndex: selectedIndices.columnIndex - 1
                  })
                  onActionButtonClick(
                    'ml',
                    selectedIndices.rowIndex,
                    selectedIndices.columnIndex,
                    e,
                    false
                  )
                }}
              >
                <div>
                  <FaChevronLeft/>
                </div>
                <div>Move Left Column</div>
              </MakeenFormSquareActionButton>
              <MakeenFormSquareActionButton
                disabled={
                  selectedIndices.columnIndex ===
                  selectedIndices.columnCount - 1
                }
                onClick={(e) => {
                  setSelectedIndices({
                    ...selectedIndices,
                    columnIndex: selectedIndices.columnIndex + 1
                  })
                  onActionButtonClick(
                    'mr',
                    selectedIndices.rowIndex,
                    selectedIndices.columnIndex,
                    e,
                    false
                  )
                }}
              >
                <div>
                  <FaChevronRight/>
                </div>
                <div>Move Right Column</div>
              </MakeenFormSquareActionButton>
            </div>

            <MakeenFormHeaderField>Add new column</MakeenFormHeaderField>
            <MakeenFormDividerField />
            <div
              style={{
                display: 'grid',
                gridTemplateColumns: '1fr 1fr 1fr 1fr',
                gridGap: '8px'
              }}
            >
              {Object.keys(controls).map((k, ki) => {
                return (
                  <React.Fragment key={ki}>
                    {controls[k].map((c, ci) => {
                      return (
                        <MakeenFormSquareActionButton
                          key={`${ki}-${ci}`}
                          onClick={() => {
                            setDrawerOpen(false)
                            if (onAddColumn)
                              onAddColumn(c, selectedIndices.rowIndex)
                          }}
                        >
                          <div>
                            <c.icon/>
                          </div>
                          <div>{c.display}</div>
                        </MakeenFormSquareActionButton>
                      )
                    })}
                  </React.Fragment>
                )
              })}
            </div>
          </MakeenFormPaperVerticalPadding>
        ) : null}
      </MakeenFormDrawer>
    </form>
    </div>
  )
}

// Decorate with redux-form
export default reduxForm({
  form: 'fieldLevelValidation', // a unique identifier for this form\
  // asyncValidate,
  destroyOnUnmount: true,
  enableReinitialize: true,
  touchOnChange: true,
  trigger: 'blur'
})(FieldLevelValidationForm)