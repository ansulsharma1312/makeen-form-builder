import * as React from 'react'

import {
  MakeenFormControlEdit,
  FVBannerImage,
  FVFormBanner,
  FVFormBannerDefault,
  FVFormContainer,
  FVFormWrapper,
  FVTitleField
} from '../../components/styled'
import { Fragment, useEffect, useState } from 'react'

import FieldLevelValidationForm from '../../components/field-validation-form'
import { Provider } from 'react-redux'
import * as Themes from '../../themes'
import store from '../../store/store'
/* import { Values } from "redux-form-website-template"; */

import { NoContent } from '../../components//no-content'

import { ThemeProvider } from 'styled-components'

// import { dark } from './themes/dark'
import { Values } from "redux-form-website-template";



export const FormViewer = (
  {
    template,
    data,
    onChange,
    onControlValueChanged,
    controlMarker,
    editable,
    onButtonClick,
    onInject,
    theme,
    controls = [],
    baseTheme = 'dark',
    themeOverride = {},
    validator
  },
  ...rest
) => {
  const [loading, setLoading] = useState(true)
  const [finalData, setFinalData] = useState({})
  const [labelData, setLabelData] = useState({})
  const [localTemplate, setLocalTemplate] = useState({})
  const [finalTheme, setFinalTheme] = React.useState({})

  useEffect(() => {
    if (data) {
      setFinalData({ ...data })
    }
    if (template) {
      setLocalTemplate(JSON.parse(JSON.stringify(template)))
    }
    let defaultTheme = theme || Themes[baseTheme] || Themes['dark']
    const oTheme = themeOverride
    defaultTheme = Object.assign(defaultTheme, oTheme)
    setFinalTheme(defaultTheme)

    setLoading(false)
  }, [data, template.fields, theme])

  const controlValueChanged = (k, v, f, errorData) => {
    if (onControlValueChanged) {
      onControlValueChanged(k, v, f, errorData)
    }
  }

  const formValueChanged = (data, labelData, errorData) => {
    if (onChange) {
      onChange(data, labelData, errorData)
    }
  }

  const onValueChanged = (key, value, field, errorData) => {
    finalData[key] = value
    if (field?.label) {
      labelData[field.label] = value;
    }
    setFinalData({ ...finalData })
    setLabelData({ ...labelData })
    controlValueChanged(key, value, field)
    formValueChanged(finalData, labelData, errorData)
  }

  return (
    <ThemeProvider theme={finalTheme}>
      <Provider store={store}>
        <FVFormWrapper>
          <FVFormContainer
            background={localTemplate.background}
            style={{
              background: `${localTemplate.backgroundcolor}`,
              height: '100%'
            }}
          >
            {localTemplate &&
            localTemplate.fields &&
            localTemplate.fields.length ? (
              <div>
                <FieldLevelValidationForm
                  data={finalData}
                  fields={localTemplate.fields}
                  onChange={(key, value, field, errorData) => onValueChanged(key, value, field, errorData)}
                  onAddColumn={onInject}
                  controls={controls}
                  editable={editable}
                  onButtonClick={onButtonClick}
                  controlMarker={controlMarker}
                  validator={validator} />
              </div>
            ) : (
              <NoContent
                label='Your components will appear here'
                subtext='Add new component(s) and re-render'
              />
            )}
          </FVFormContainer>
        </FVFormWrapper>
      </Provider>
    </ThemeProvider>
  )
}
