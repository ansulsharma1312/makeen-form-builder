import * as React from 'react'

import {
  MakeenFormAccordion,
  MakeenFormAccordionDetails,
  MakeenFormAccordionSummary,
  MakeenFormListIcon,
  MakeenFormListItem,
  MakeenFormListItemText,
  MakeenFormPaper,
  MakeenFormSideBar
} from '../styled'
import { Fragment, useEffect, useState } from 'react'
import {
  FaChevronDown
} from 'react-icons/fa'

import { Divider, List } from '@mui/material'
export const Sidebar = ({
  controls,
  onAdd
}) => {
  const [expanded, setExpanded] = React.useState(0)
  const [anchorEl, setAnchorEl] = React.useState(null)
  const [open, setOpen] = React.useState(false)
  // let open = Boolean(anchorEl)

  const handleExpansionChange = (index) => {
    if (expanded !== index) {
      setExpanded(index)
    } else {
      setExpanded(99);
    }
  }
  const handleClickListItem = (event) => {
    setAnchorEl(event.currentTarget)
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
    setAnchorEl(null)
  }

  const onMenuClicked = (con) => {
    handleClose()
    if (onAdd) {
      onAdd(con)
    }
  }

  return (
    <>
      { <MakeenFormSideBar elevation={1} className='flex-1'>
          <MakeenFormPaper className='fp-side-bar'>
            <MakeenFormPaper className='fp-side-bar-body'>
            <>
                    {Object.keys(controls).map((fc, fci) => {
                      return (
                        <MakeenFormAccordion
                          key={fci}
                          expanded={expanded === fci}
                          onChange={() => handleExpansionChange(fci)}
                        >
                          <MakeenFormAccordionSummary
                            expandIcon={
                              <FaChevronDown/>
                            }
                            aria-controls='panel1a-content'
                            id='panel1a-header'
                          >
                            {fc}
                          </MakeenFormAccordionSummary>
                          <MakeenFormAccordionDetails>
                            <List
                              component='nav'
                              aria-label='toolbox-body'
                              style={{ width: '100%', overflow: 'auto' }}
                            >
                              {controls[fc].map((con, conti) => {
                                return (
                                  <React.Fragment key={conti}>
                                    <MakeenFormListItem
                                      dense
                                      button
                                      onClick={() => onAdd(con)}
                                    >
                                      <MakeenFormListIcon>
                                        <con.icon />
                                      </MakeenFormListIcon>
                                      <MakeenFormListItemText
                                        primary={`${con.display}`}
                                      />
                                    </MakeenFormListItem>
                                    <Divider />
                                  </React.Fragment>
                                )
                              })}
                            </List>
                          </MakeenFormAccordionDetails>
                        </MakeenFormAccordion>
                      )
                    })}
                  </>
            </MakeenFormPaper>
          </MakeenFormPaper>
        </MakeenFormSideBar>}
    </>
  )
}
