import * as React from 'react'
import { useState, useEffect } from 'react'
import {
  MakeenFormToolButton,
  MakeenFormPaperVerticalPadding
} from '../../components/styled'

import {FaTimes, FaSave } from 'react-icons/fa'
import { FormViewer } from '../form-builder/FormViewer'
import { Grid } from '@mui/material'

export const FormProperties = ({ data, onChange, onClose }) => {
  const [formProperties, setFormProperties] = useState({})

  // const [dragState, setDragState] = useState({ quotes: initial });

  useEffect(() => {
    const subset = (({
      title,
      description,
      banner,
      background,
      backgroundcolor,
      bannercolor
    }) => ({
      title,
      description,
      banner,
      background,
      backgroundcolor,
      bannercolor
    }))(data)
    setFormProperties(subset)
  }, [data])

  const onValueChanged = (key, value) => {
    formProperties[key] = value
    setFormProperties(formProperties)
  }

  // Event Handlers
  const onSaveClicked = () => {
    if (onChange) {
      onChange(formProperties)
    }
  }

  const onFormValueChanged = (key, value, field) => {
    formProperties[key] = value
    setFormProperties({ ...formProperties })
  }

  // Rendering
  return (
    <MakeenFormPaperVerticalPadding style={{ height: '100%' }}>
      <div>
        <MakeenFormMediumHeaderBar>
          <div className='header-title'>
            <MakeenFormMediumHeader>Form Properties</MakeenFormMediumHeader>
          </div>
          <div className='header-tool-bar'>
            <MakeenFormToolButton
              variant='contained'
              size='large'
              aria-label='save'
              onClick={() => onSaveClicked()}
              anchor={'bottom'}
              style={{ width: '150px' }}
            >
              <FaSave/>
              <span>Save</span>
            </MakeenFormToolButton>
            <MakeenFormToolButton
              variant='contained'
              size='large'
              aria-label='move down'
              onClick={() => onClose()}
              anchor={'bottom'}
              style={{ width: '150px' }}
            >
              <FaTimes/>
              <span>Cancel</span>
            </MakeenFormToolButton>
          </div>
        </MakeenFormMediumHeaderBar>
        <div style={{ display: 'flex', flex: 1 }}>
          <FormViewer
            data={formProperties}
            onChange={(key, value, field) => {
              onFormValueChanged(key, value, field)
            }}
            template={{
              fields: [
                [
                  {
                    type: 'header',
                    value: 'Update form properties'
                  }
                ],
                [
                  { type: 'text', datafield: 'title', label: 'Title' },
                  {
                    type: 'text',
                    datafield: 'description',
                    label: 'Description'
                  }
                ],
                [
                  {
                    type: 'imageupload',
                    datafield: 'banner',
                    label: 'Banner Image'
                  },
                  {
                    type: 'imageupload',
                    datafield: 'background',
                    label: 'Background Image'
                  }
                ],
                [
                  {
                    type: 'color',
                    datafield: 'bannercolor',
                    label: 'Banner Color'
                  },
                  {
                    type: 'color',
                    datafield: 'backgroundcolor',
                    label: 'Background Color'
                  }
                ]
              ]
            }}
          />
        </div>
      </div>
    </MakeenFormPaperVerticalPadding>
  )
}
