import * as React from 'react'

import {
  MakeenFormPaperVerticalPadding,
  MakeenFormTabWrapper,
  MakeenFormTabPanel,
  MakeenFormTabs,
  MakeenFormTab,
  MakeenFormFiedlSet,
  MakeenFormToolButton,
  MakeenFormMediumHeaderBar,
  MakeenFormMediumHeader
} from '../styled'
import {
  FaSave,
  FaTimes,
  FaEye,
  FaEdit
} from 'react-icons/fa'
import { useEffect, useState } from 'react'

import { Box } from '@mui/material'
import { getFinalField } from './fields'

import { getControlTemplate } from '../../services/control-template'
import { FormViewer } from '../form-builder/FormViewer'

function TabPanel(props) {
  const { children, value, index, ...other } = props

  return (
    <MakeenFormTabPanel
      role='tabpanel'
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={0}>{children}</Box>}
    </MakeenFormTabPanel>
  )
}

export const PropertyEditor = ({
  controls,
  template,
  index,
  validate,
  onChange,
  onClose,
  plannerConfig = { plannerConfig }
}) => {
  const [controlState, setControlState] = useState({})
  const [editContainerGroups, setEditContainerGroups] = useState({})
  const [readOnlyTemplate, setReadOnlyTemplate] = useState(undefined)
  const [selectedTabIndex, setSelectedTabIndex] = React.useState(0)
  const [selectedIndex, setSelectedIndex] = React.useState(index)
  const [finalData, setFinalData] = React.useState({})
  const [errorData, setErrorData] = React.useState([])
  const [validator, setValidator] = React.useState(false)

  useEffect(() => {
    if (validate) {
      setValidator(true)
      index = {row: 0, column: 1}
    }
    if (controls && controls.length) {
      let control = Object.assign({}, controls[index.row][index.column])

      const res = {}
      if (!control.editableFields) {
        control = Object.assign(
          control,
          getControlTemplate(control, control, plannerConfig)
        )
      }
      control.editableFields.forEach((ed) => {
        const groupName = ed[0].group ? ed[0].group : 'Default'
        ed.value = controls[index.row][index.column][ed.datafield]
        if (!res[groupName]) {
          res[groupName] = [ed]
        } else {
          res[groupName].push(ed)
        }
      })
      setReadOnlyTemplate(controls)
      setControlState(Object.assign({}, controls[index.row][index.column]))
      setEditContainerGroups(res)
      console.log(validate)
    }
  }, [])

  const handleTabSelectionChange = (_event, newValue) => {
    setSelectedTabIndex(newValue)
  }

  const onValueChanged = (_key, value, field) => {
    if (field.is_custom) {
      controlState.custom = controlState.custom || {}
      controlState.custom[field.datafield] =
        controlState.custom[field.datafield] || {}
      let vals = {}
      if (typeof value === 'object') {
        vals = value;
      } else {
        value.forEach((v) => {
          vals[v.name] = v.value
        })
      }
      
      
      if (field.isappend) {
        controlState.custom[field.datafield] = Object.assign(
          controlState.custom[field.datafield] || {},
          vals
        )
      } else {
        controlState.custom[field.datafield] = vals
      }
    } else {
      if (field.isappend) {
        controlState[field.datafield] = Object.assign(
          controlState[field.datafield] || {},
          value
        )
      } else {
        controlState[field.datafield] = value
      }
    }
    setControlState({ ...controlState })
    return controlState
  }

  const onSave = () => {
    if (onChange) {
      onChange(readOnlyTemplate)
    }
    onCancel()
  }

  const onCancel = () => {
    if (onClose) {
      onClose()
    }
  }

  const getFieldControl = (field) => {
    return getFinalField(
      field,
      onValueChanged,
      controlState[field.datafield],
      undefined,
      field.datafield
    )
  }

  const onControlValueChanged = (key, value, field) => {
    const res = onValueChanged(key, value, field)
    readOnlyTemplate[selectedIndex.row][selectedIndex.column][key] = value
    setReadOnlyTemplate(JSON.parse(JSON.stringify(readOnlyTemplate)))
  }

  const onChangeFinalData = (finalData, labelData, errorData) => {    
    setFinalData(finalData);
    setErrorData(errorData);
  }

  return (
    <MakeenFormPaperVerticalPadding style={{ height: '100%' }}>
      <div>
        <MakeenFormMediumHeaderBar>
          <div className='header-title'>
            <MakeenFormMediumHeader>Type: {controlState.typeDisplay}</MakeenFormMediumHeader>
          </div>
          {!validator ? <div className='header-tool-bar'>
            <MakeenFormToolButton
              variant='contained'
              size='large'
              aria-label='save'
              onClick={() => onSave()}
              anchor={'bottom'}
              style={{ width: '150px' }}
            >
              <FaSave/>
              <span>Save</span>
            </MakeenFormToolButton>
            <MakeenFormToolButton
              variant='contained'
              size='large'
              aria-label='move down'
              onClick={() => onCancel()}
              anchor={'bottom'}
              style={{ width: '150px' }}
            >
              <FaTimes/>
              <span>Cancel</span>
            </MakeenFormToolButton>
          </div> : 
          <MakeenFormToolButton
              variant='contained'
              size='large'
              aria-label='move down'
              onClick={() => onCancel()}
              anchor={'bottom'}
              style={{ width: '150px' }}
            >
              <FaTimes/>
              <span>Cancel</span>
            </MakeenFormToolButton>}
          
        </MakeenFormMediumHeaderBar>
      </div>
      <div style={{ display: 'flex', flex: 1 }}>
        <div style={{ width: '30%', padding: '8px' }}>
          <MakeenFormFiedlSet>
            <legend>
              <FaEye /> <span>Preview</span>
            </legend>
            <div
              style={{
                display: 'flex',
                flex: 1,
                zoom: '50%'
              }}
            >
              <FormViewer
                template={{ ...{ fields: readOnlyTemplate } }}
                controlMarker={selectedIndex}
                onChange={onChangeFinalData}
                validator={validator}
              />
            </div>
          </MakeenFormFiedlSet>
        </div>
        <div style={{ flex: 1, padding: '8px' }}>
          {!validator ? 
          <MakeenFormFiedlSet>
            <legend>
              <FaEdit/> <span>Properties</span>
            </legend>
            <MakeenFormTabWrapper>
              <MakeenFormTabs
                variant='scrollable'
                value={selectedTabIndex}
                onChange={handleTabSelectionChange}
                aria-label='Control editor'
                indicatorColor='primary'
                textColor='primary'
              >
                {Object.keys(editContainerGroups).map((group, groupi) => {
                  return (
                    <MakeenFormTab
                      label={`${group}`}
                      key={groupi}
                      selected={selectedTabIndex === groupi}
                    />
                  )
                })}
              </MakeenFormTabs>
              <div style={{ overflow: 'auto', flex: 1 }}>
                {Object.keys(editContainerGroups).map((group, groupi) => {
                  return (
                    <TabPanel
                      key={groupi}
                      value={selectedTabIndex}
                      index={groupi}
                      style={{ flex: 1, padding: '8px', height: '100%' }}
                    >
                      {
                        <FormViewer
                          template={{ fields: editContainerGroups[group] }}
                          data={controlState}
                          onControlValueChanged={onControlValueChanged}
                        />
                      }
                    </TabPanel>
                  )
                })}
              </div>
            </MakeenFormTabWrapper>
          </MakeenFormFiedlSet> :
          <div>
            <MakeenFormFiedlSet style={{height: '250px', overflow: 'auto'}}>
              <legend>
                <span>Data</span>
              </legend>
              <pre style={{color: '#fff'}}>{JSON.stringify(finalData, null, "\t")}</pre>
            </MakeenFormFiedlSet>
            <MakeenFormFiedlSet style={{height: '250px', overflow: 'auto'}}>
              <legend>
                <span>Errors</span>
              </legend>
              <pre style={{color: 'red'}}>{JSON.stringify(errorData, null, "\t")}</pre>
            </MakeenFormFiedlSet>
          </div>
          }
        </div>
      </div>
    </MakeenFormPaperVerticalPadding>
  )
}
