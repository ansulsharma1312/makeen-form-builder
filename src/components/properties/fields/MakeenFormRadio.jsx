import * as React from 'react'
import { useState, useEffect } from 'react'
import {
  MakeenFormRadio,
  MakeenFormFormControlLabel,
  MakeenFormFormLabel,
  MakeenFormRadioGroup
} from '../../styled'
import { FormControl } from '@mui/material'

const MakeenFormRadioControl = ({ field, onChange, ...props }) => {
  const [localValue, setLocalvalue] = useState(field.value)

  const onValueChanged = (e) => {
    setLocalvalue(e.target.value)
    if (onChange) {
      const retValue = field.data.filter((f) => {
        return f.value.toString() === e.target.value.toString()
      })
      if (retValue && retValue.length && retValue[0].returnvalue) {
        onChange(field, retValue[0].returnvalue, field)
      } else {
        onChange(field, e.target.value, field)
      }
    }
  }

  useEffect(() => {}, [])

  return (
    <div>
      <FormControl component='fieldset'>
        {field.label ? (
          <MakeenFormFormLabel component='legend'>{field.label}</MakeenFormFormLabel>
        ) : null}

        <MakeenFormRadioGroup
          aria-label={field.datafield}
          name={field.datafield}
          value={localValue}
          onChange={onValueChanged}
        >
          {field.data.map((d, di) => {
            return (
              <MakeenFormFormControlLabel
                key={di}
                value={d.value}
                control={<MakeenFormRadio {...props} />}
                label={d.name}
              />
            )
          })}
        </MakeenFormRadioGroup>
      </FormControl>
    </div>
  )
}

export default MakeenFormRadioControl
