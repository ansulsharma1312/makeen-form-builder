import * as React from 'react'

import {
  MakeenFormCheckbox,
  MakeenFormFieldSet,
  MakeenFormFormControlLabel,
  MakeenFormFormLabel,
  MakeenFormHeaderField,
  MakeenFormLabelField,
  MakeenFormNoContentAvailable,
  MakeenFormTextField
} from '../../styled'
import { FaImage } from 'react-icons/fa'

import MakeenFormRadioControl from './MakeenFormRadio'

// import MakeenFormPdfViewer from "./MakeenFormPdfViewer";

const getStyleObject = (style) => {
  const res = {}
  if (style) {
    style.forEach((s) => {
      res[s['name']] = s['value']
    })
  }
  return res
}
export const getFinalField = (
  infield,
  onValueChange,
  invalue,
  label,
  fieldname
) => {
  let resComponent = {}
  const field = Object.assign({}, infield)
  const strFieldName = fieldname ? fieldname : field.datafield
  if (!field.custom) {
    field.custom = {
      style: [],
      props: []
    }
  }

  const value = invalue || field.value

  const localprops = {
    style: getStyleObject(field.custom.style),
    ...field.custom.props
  }
  switch (field.type) {
    // case 'richeditor':
    //   const modules = {
    //     toolbar: [
    //       [{ header: [1, 2, false] }],
    //       ['bold', 'italic', 'underline', 'strike', 'blockquote'],
    //       [
    //         { list: 'ordered' },
    //         { list: 'bullet' },
    //         { indent: '-1' },
    //         { indent: '+1' }
    //       ],
    //       ['clean']
    //     ]
    //   }
    //   const formats = [
    //     'header',
    //     'bold',
    //     'italic',
    //     'underline',
    //     'strike',
    //     'blockquote',
    //     'list',
    //     'bullet',
    //     'indent'
    //   ]
    //   resComponent = (
    //     <MakeenFormRichTextEditor
    //       value={value}
    //       theme={'snow'}
    //       placeholder={field.placeholder}
    //       modules={modules}
    //       formats={formats}
    //       bounds={'.app'}
    //       onChange={(e) => {
    //         if (onValueChange) {
    //           onValueChange(strFieldName, e, field)
    //         }
    //       }}
    //     />
    //   )
    //   break
    case 'text':
      resComponent = (
        <MakeenFormTextField
          id={`text-field-${strFieldName}`}
          label={`${label || field.label}`}
          value={value}
          onChange={(e) => {
            if (onValueChange) {
              onValueChange(strFieldName, e.target.value, field)
            }
          }}
          type={field.subtype || 'text'}
          fullWidth
          size='small'
          variant='outlined'
          {...localprops}
        />
      )
      break
    case 'divider':
      resComponent = <hr className='MuiDivider-root' {...localprops} />
      break
    case 'header':
      resComponent = (
        <MakeenFormHeaderField {...localprops}>{label || field.label}</MakeenFormHeaderField>
      )
      break
    case 'label':
      resComponent = (
        <MakeenFormLabelField
          dangerouslySetInnerHTML={{ __html: label || field.label }}
          {...localprops}
        ></MakeenFormLabelField>
      )
      break
    case 'radio':
      resComponent = (
        <MakeenFormRadioControl
          onChange={(fld, val, fielddata) => {
            if (onValueChange) {
              onValueChange(fld, val, fielddata)
            }
          }}
          field={field}
          rows={value && value.length ? value : []}
          {...localprops}
        ></MakeenFormRadioControl>
      )
      break
    case 'checkbox':
      resComponent = (
        <div>
          <MakeenFormFormControlLabel
            control={
              <MakeenFormCheckbox
                checked={value}
                onChange={(e) => {
                  if (onValueChange) {
                    onValueChange(strFieldName, e.target.checked, field)
                  }
                }}
                name='checkedB'
                {...localprops}
              />
            }
            label={`${label || field.label}`}
          ></MakeenFormFormControlLabel>
        </div>
      )
    case 'textarea':
      resComponent = (
        <MakeenFormTextField
          label={`${label || field.label}`}
          multiline
          value={value}
          onChange={(e) => {
            if (onValueChange) {
              onValueChange(strFieldName, e.target.value, field)
            }
          }}
          fullWidth
          size='small'
          variant='outlined'
          {...localprops}
        />
      )
      break

    case 'image':
      resComponent = (
        <React.Fragment>
          {value && value.length ? (
            <React.Fragment>
              {value.map((f, fi) => {
                return (
                  <div key={fi}>
                    <img src={`${f}`} alt={f} {...localprops}></img>
                  </div>
                )
              })}
            </React.Fragment>
          ) : (
            <MakeenFormNoContentAvailable>
              <div>
                <FaImage/>
              </div>
              <div>NO IMAGE SELECTED</div>
            </MakeenFormNoContentAvailable>
          )}
        </React.Fragment>
      )
      break
    // case "pdf": resComponent = (
    //   <React.Fragment>
    //     {value ? (<MakeenFormPdfViewer value={value} />) : (
    //       <MakeenFormNoContentAvailable>
    //         <div><FontAwesomeIcon icon={faFilePdf} /></div>
    //         <div >NO PDF FILE SELECTED</div>
    //       </MakeenFormNoContentAvailable>
    //     )}
    //   </React.Fragment>

    // )
    //   break;

    default:
      resComponent = <React.Fragment></React.Fragment>
  }
  return resComponent
}
