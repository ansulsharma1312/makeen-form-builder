import * as React from 'react'

import {
  MakeenFormFieldSet,
  MakeenFormGridActionCell,
  MakeenFormGridCell,
  MakeenFormGridHeaderRow,
  MakeenFormGridRow,
  MakeenFormTextField,
  MakeenFormToolButton,
  SmallHeader
} from '../styled'
import { Fragement, useEffect, useState } from 'react'
import { FaPlus, FaTrash } from 'react-icons/fa'

import { Button } from '@mui/material'
import { v4 as uuidv4 } from 'uuid'

export const MakeenFormDataGridView = (
  {
    field,
    value,
    editable,
    inputvalue,
    onValueChanged,
    required,
    input,
    meta: { asyncValidating, touched, error }
  },
  ...rest
) => {
  const [gridRows, setGridRows] = useState([])
  const [gridColumns, setGridColumns] = useState([])
  const [loading, setLoading] = useState(true)

  const onValueUpdate = (index, fieldName, fieldValue) => {
    const res = gridRows[index]
    res[fieldName] = fieldValue
    setGridRows(JSON.parse(JSON.stringify(gridRows)))
    if (onValueChanged) {
      if (field.asobject) {
        const resObj = {}
        gridRows.forEach((r) => {
          resObj[r.name] = r.value
        })
        onValueChanged(field.datafield, resObj, field)
      } else {
        onValueChanged(field.datafield, gridRows, field)
      }
    }
  }

  useEffect(() => {
    let gcolumns = []
    if (field.columns && field.columns.length) {
      setGridColumns(field.columns)
      gcolumns = field.columns
    } else {
      gcolumns = [
        { field: 'name', headerName: 'Property Name' },
        { field: 'value', headerName: 'Value' }
      ]
      setGridColumns(gcolumns)
    }
    if (inputvalue) {
      if (!field.asobject) {
        // const r = []
        // if (typeof inputvalue === 'object') {
        //   Object.keys(inputvalue).forEach((k) => {
        //     r.push({
        //       [gcolumns[0].field]: k,
        //       [gcolumns[0].value]: inputvalue[k]
        //     })
        //   })
        //   setGridRows(r)
        // } else {
        setGridRows(inputvalue)
        // }
      } else {
        const res = []
        Object.keys(inputvalue).forEach((k) => {
          res.push({
            name: k,
            value: input.value[k]
          })
        })
        setGridRows(res)
      }
    }
    setLoading(false)
  }, [field])

  const onAdd = () => {
    const newRow = {
      value: field.aslist ? uuidv4() : ''
    }
    gridColumns.forEach((gc) => {
      newRow[gc['field']] = newRow[gc['field']] || ''
    })
    gridRows.push(newRow)
    setGridRows(gridRows.concat([]))
  }

  const onRemove = (index) => {
    gridRows.splice(index, 1)
    setGridRows(JSON.parse(JSON.stringify(gridRows)))
  }
  return (
    <div>
      {loading ? (
        <div></div>
      ) : (
        <MakeenFormFieldSet bordered>
          <legend>{field.label}</legend>
          <React.Fragment>
            {gridRows.map((_gr, gri) => {
              return (
                <MakeenFormGridRow key={gri}>
                  <MakeenFormGridActionCell>
                    {gri < gridRows.length ? (
                      <Button
                        size='small'
                        color='secondary'
                        aria-label='save'
                        onClick={() => {
                          if (!editable) {
                            onRemove(gri)
                          }
                        }}
                      >
                        <FaTrash/>
                      </Button>
                    ) : null}
                  </MakeenFormGridActionCell>
                  <React.Fragment>
                    {gridColumns.map((gc, gci) => {
                      return (
                        <div key={gci} style={{ flex: gc.hide ? 0 : 1 }}>
                          {!gc.hide ? (
                            <MakeenFormGridCell key={gci}>
                              <MakeenFormTextField
                                size='small'
                                value={gridRows[gri][gc['field']]}
                                label={`${gc['headerName']}`}
                                placeholder={`update ${gc['field']}`}
                                onChange={(e) => {
                                  if (!editable) {
                                    onValueUpdate(
                                      gri,
                                      gc['field'],
                                      e.target.value
                                    )
                                  }
                                }}
                              />
                            </MakeenFormGridCell>
                          ) : (
                            <span style={{ flex: 0 }}></span>
                          )}
                        </div>
                      )
                    })}
                  </React.Fragment>
                </MakeenFormGridRow>
              )
            })}
          </React.Fragment>
          <MakeenFormGridRow>
            <MakeenFormGridActionCell>
              <div>
                <MakeenFormToolButton
                  variant='contained'
                  anchor='bottom'
                  onClick={() => {
                    if (!editable) {
                      onAdd()
                    }
                  }}
                >
                  <FaPlus/>
                  <span>Add</span>
                </MakeenFormToolButton>
              </div>
            </MakeenFormGridActionCell>
          </MakeenFormGridRow>
        </MakeenFormFieldSet>
      )}
    </div>
  )
  return <Fragement></Fragement>
}
