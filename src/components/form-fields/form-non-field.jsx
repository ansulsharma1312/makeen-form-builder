import * as React from 'react'
import { useState, useEffect, Fragement } from 'react'
import {
  MakeenFormNoContentAvailable,
  MakeenFormDividerField,
  FVImageContainer,
  FVLabelField
} from '../styled'
import { FVPDFViewer } from './pdf-field'
import { FaImage } from 'react-icons/fa'

export const FVFormNonField = (props) => {
  const renderSwitch = () => {
    switch (props.field.type) {
      case 'image':
        return (
          <FVImageContainer {...props.field.props}>
            {props.field.value && props.field.value.length ? (
              <img
                src={props.field.value}
                style={{ maxWidth: '100%', maxHeight: '100%' }}
              />
            ) : (
              <MakeenFormNoContentAvailable>
                <div>
                  <FaImage/>
                </div>
                <div>NO IMAGE SELECTED</div>
              </MakeenFormNoContentAvailable>
            )}
          </FVImageContainer>
        )
      case 'divider':
        return <MakeenFormDividerField {...props} />
      case 'label':
        return (
          <FVLabelField
            dangerouslySetInnerHTML={{ __html: props.field.value }}
            {...props}
          ></FVLabelField>
        )
      case 'pdf':
        return (
          <FVPDFViewer
            field={props.field}
            {...props}
            style={JSON.parse(JSON.stringify(props.field.style || {}))}
          ></FVPDFViewer>
        )
      default:
        return 'foo'
    }
  }

  return <React.Fragment>{renderSwitch()}</React.Fragment>
  //   <div>
  //     <label>{label}</label>
  //     <div>
  //       <input {...input} placeholder={label} type={type} />
  //       {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
  //     </div>
  //   </div>
  // )
}
